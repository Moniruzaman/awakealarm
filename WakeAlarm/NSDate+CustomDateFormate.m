//
//  NSDate+CustomDateFormate.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/10/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import "NSDate+CustomDateFormate.h"

@implementation NSDate (CustomDateFormate)

+(NSString *)getTodayDateWithStringFomate{

    NSDateFormatter *dateFormatter=[[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMM yyyy | hh:mm a"];
    //NSLog(@"%@",[dateFormatter stringFromDate:[NSDate date]]);
    return [dateFormatter stringFromDate:[NSDate date]];
}
@end
