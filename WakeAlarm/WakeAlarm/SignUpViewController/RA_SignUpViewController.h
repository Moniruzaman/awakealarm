//
//  RA_SignUpViewController.h
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/6/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"

@interface RA_SignUpViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate>{
    __weak IBOutlet UIView *navBarBottomView;
    __weak IBOutlet UIImageView *profilePictureImageView;
    __weak IBOutlet UIButton *btnSetImage;
    
    __weak IBOutlet UITextField *txtFname;
    __weak IBOutlet UITextField *txtLname;
    __weak IBOutlet UITextField *txtEmailID;
    __weak IBOutlet UITextField *txtPassword;
    __weak IBOutlet UITextField *txtUserName;
    __weak IBOutlet UITextField *txtAddress;
    
    __weak IBOutlet UIButton *btnMale;
    __weak IBOutlet UIButton *btnFemale;
    
    
}
- (IBAction)setImageAction:(id)sender;
-(IBAction)chooseGender:(id)sender;
@end
