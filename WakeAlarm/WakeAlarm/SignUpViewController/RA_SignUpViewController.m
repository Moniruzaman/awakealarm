
//
//  RA_SignUpViewController.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/6/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import "RA_SignUpViewController.h"

@interface RA_SignUpViewController ()

@end

@implementation RA_SignUpViewController
bool imageSet = false;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initialViewSetUp];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:RGB(255, 0, 0)];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [self setTitle:SIGN_UP_VIEW];
    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSFontAttributeName: [UIFont fontWithName:@"Montserrat-Light" size:20.0f],NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    self.navigationItem.leftBarButtonItem  = [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"LBarImage.png"] target:self action:@selector(leftBarButtonItemAction)];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"save-icon.png"] target:self action:@selector(rightBarButtonItemAction)];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:UIKeyboardWillHideNotification object:nil];
}
- (void)viewWillDisappear:(BOOL)animated {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}
-(void)initialViewSetUp{
    
    [navBarBottomView setBackgroundColor:RGB(168, 32, 32)];
    [self.view setBackgroundColor:RGB(255, 0, 0)];
    [profilePictureImageView.layer setCornerRadius:65];
    [profilePictureImageView setClipsToBounds:YES];
    [profilePictureImageView.layer setBorderWidth:10];
    [profilePictureImageView.layer setBorderColor:RGB(255, 207, 6).CGColor];
    [profilePictureImageView setImage:[UIImage imageNamed:@"boy-icon.png"]];
    
    [btnSetImage.layer setCornerRadius:3.00];
    [btnSetImage setClipsToBounds:YES];
    [btnSetImage setBackgroundColor:RGB(253, 206, 12)];
    [btnSetImage setTitleColor:RGB(168, 32, 32) forState:UIControlStateNormal];
    
    [btnMale   setImage:[UIImage imageNamed:@"Cross.png"] forState:UIControlStateSelected];
    [btnFemale setImage:[UIImage imageNamed:@"Cross.png"] forState:UIControlStateSelected];
    [btnMale  setImage: [UIImage imageNamed:@""] forState:UIControlStateNormal];
    [btnFemale setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    
    for (NSObject *obj in [self.view subviews]) {
        if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *lbl = (UILabel *)obj;
            [lbl setFont:[UIFont fontWithName:@"Montserrat-Light" size:14]];
        }
        else if ([obj isKindOfClass:[UITextField class]])
        {
            UITextField *txt = (UITextField *)obj;
            txt.delegate = self;
            [txt setTextColor:RGB(168, 32, 32)];
        }
    }
    
}
-(IBAction)chooseGender:(id)sender
{
    if ([sender isSelected]) {
        [sender setSelected:NO];
    }
    else
    {
        if ([sender tag]==1) {
            [btnFemale isSelected] ? 0 : [sender setSelected:YES];
        }
        else
        {
            [btnMale isSelected] ? 0 : [sender setSelected:YES];
        }
    }
}
- (IBAction)setImageAction:(id)sender {
    BOOL cameraAvailable =[UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
    if(!cameraAvailable) {
        // running on simulator or something else without camera
        NSLog(@"camera is not found on this device");
    }
    else {
        // camera exists, you can alert
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:picker animated:YES completion:NULL];
        
    }
}
-(BOOL)checkFieldsNullValue
{
    Boolean return_Value = false;
    for (NSObject *obj in [self.view subviews]) {
     
        if ([obj isKindOfClass:[UITextField class]])
        {
            UITextField *txt = (UITextField *)obj;
            if ([txt.text isEqualToString:@""]) {
                [txt.layer setBorderWidth:2];
                [txt.layer setBorderColor:[UIColor blueColor].CGColor];
                return_Value = true;
            }
        }
        else if([obj isKindOfClass:[UIButton class]])
        {
            UIButton *btn = (UIButton *)obj;
            if ([btn tag]==1||[btn tag]==2) {
                if ([btn isSelected]) {
                    return false;
                }
                else
                {
                    return_Value = true;
                }
            }
        }
    }
    return return_Value;
}
#pragma mark - keyboard movements
- (void)keyboardWillShow:(NSNotification *)notification
{
    CGSize keyboardSize = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue].size;
    
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = -keyboardSize.height+BANNER_HEIGHT+STATUS_BAR_HEIGHT;
        self.view.frame = f;
    }];
}

-(void)keyboardWillHide:(NSNotification *)notification
{
    [UIView animateWithDuration:0.3 animations:^{
        CGRect f = self.view.frame;
        f.origin.y = 0.0f;
        self.view.frame = f;
    }];
}

#pragma -mark UiImagepickerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    profilePictureImageView.image = chosenImage;
    imageSet = true;

    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
#pragma -mark barButtonActions
-(void)leftBarButtonItemAction
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)rightBarButtonItemAction
{
    if ([self checkFieldsNullValue]) {
        [[AppDelegate shareDelegate] customAlert:@"Warning!" withMessage:@"Please fill all the Information."];
    }
    else
    {
        if ([[MyManager sharedManager] validateEmail:txtEmailID.text]) {
            NSString *gender = [btnMale isSelected] ? @"Male" : @"Female";
            NSData *imageData = UIImagePNGRepresentation(profilePictureImageView.image);
            NSDictionary *userInfoDataDict = [[NSDictionary alloc] initWithObjectsAndKeys:@"525",@"id",txtFname.text,@"fname",txtLname.text,@"lname",gender,@"sex",txtEmailID.text,@"email",txtPassword.text,@"password",txtUserName.text,@"username",txtAddress.text,@"address",DEFAULT_PROFILE_STATUS,@"profileStatus",@"0",@"tactiveschedule",@"0",@"tfriend",@"0",@"talarmfriend",imageData,@"profileIamgeData", imageSet ? @"Set" : @"UnSet",@"imagesetstatus",nil];
            
            if (is_Debug) {
                NSLog(@"%@",userInfoDataDict);
            }
            if ([[MyManager sharedManager] writeInformationEmailIDWithPassword:txtEmailID.text :txtPassword.text :userInfoDataDict]) {
                [[AppDelegate shareDelegate] customAlert:@"Success!" withMessage:@"Data has been saved successfully"];
                
                //go to menu view
                NSDictionary *dataDictionary = [[MyManager sharedManager] retrieveInformationEmailIDWIthPassword:txtEmailID.text :txtPassword.text];
                UserInfo *user = [[UserInfo alloc] initWithValueFromDictionary:dataDictionary];
                [[MyManager sharedManager] setActiveUserInformation:user];
                //Store EmailID and Password to Cash NSuserDefault
                [[NSUserDefaults standardUserDefaults] setObject:[[MyManager sharedManager] activeUserInformation].email forKey:@"email"];
                [[NSUserDefaults standardUserDefaults] setObject:[[MyManager sharedManager] activeUserInformation].password forKey:@"password"];
                [[NSUserDefaults standardUserDefaults] setObject:[[MyManager sharedManager] activeUserInformation].userName forKey:@"username"];
                
                //[self.navigationController popViewControllerAnimated:YES];
                [[NSNotificationCenter defaultCenter] postNotificationName:GOTO_MENU_VIEW_AFTER_SIGNUP_DONE object:nil];
            }
            else{
                
                [[AppDelegate shareDelegate] customAlert:@"Error!" withMessage:@"Failed to save Data"];
            }
        }
        else
        {
            [[AppDelegate shareDelegate] customAlert:@"Warning!" withMessage:@"Please provide your email in a correct format"];
        }
    }
}
#pragma -mark UITextFieldDelegateMethod
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    
    [textField resignFirstResponder];
    for (NSObject *obj in [self.view subviews]) {
        if ([obj isKindOfClass:[UITextField class]])
        {
            UITextField *txt = (UITextField *)obj;
            if (![txt.text isEqualToString:@""])
            {
                [txt.layer setBorderWidth:0];
                [txt.layer setBorderColor:nil];
            }
        }
    }
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
@end
