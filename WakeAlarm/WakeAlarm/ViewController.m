//
//  ViewController.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/4/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import "ViewController.h"
#import "PrefixHeader.pch"

@interface ViewController ()

@end

@implementation ViewController
@synthesize enterAppBtn;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [enterAppBtn.layer setCornerRadius:3.00];
    [enterAppBtn setClipsToBounds:YES];
    [enterAppBtn setBackgroundColor:RGB(253, 206, 12)];
    [enterAppBtn setTitleColor:RGB(168, 32, 32) forState:UIControlStateNormal];
    self.navigationController.navigationBarHidden = YES;
    
    if (is_Debug) {
        for (NSString* family in [UIFont familyNames]) {
            NSLog(@"%@", family);
            
            for (NSString* name in [UIFont fontNamesForFamilyName: family]) {
                NSLog(@"  %@", name);
            }
        }
    }
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)enterBtnAction:(id)sender {
    
    RA_MenuViewController *controller = [[RA_MenuViewController alloc] initWithNibName:@"RA_MenuViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}
@end
