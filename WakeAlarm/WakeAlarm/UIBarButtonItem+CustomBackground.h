//
//  UIBarButtonItem+CustomBackground.h
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/5/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIBarButtonItem (CustomBackground)
{
}
+ (UIBarButtonItem*)barItemWithImage:(UIImage*)image target:(id)target action:(SEL)action;
@end
