//
//  AppDelegate.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/4/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate ()

@end

@implementation AppDelegate
@synthesize bannerView;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    //UserInfo *user = [[UserInfo alloc] initWithValueFromDictionary:USER_INFO_DIC];
    //[[MyManager sharedManager] setActiveUserInformation:user];
    //sleep(10);
    
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);

    ViewController *view=[[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    UINavigationController *nav=[[UINavigationController alloc] initWithRootViewController:view];
    self.window.rootViewController=nav;
    [self.window makeKeyAndVisible];
    
    bannerView = [[GADBannerView alloc] initWithFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-BANNER_HEIGHT, [[UIScreen mainScreen] bounds].size.width, BANNER_HEIGHT)];
    [bannerView setBackgroundColor:[UIColor whiteColor]];
    [self.window addSubview:bannerView];
    bannerView.adUnitID = @"ca-app-pub-7117592313772627/3953400195";
    bannerView.rootViewController = nav;
    [bannerView loadRequest:[GADRequest request]];
    
    return YES;
}
+(AppDelegate*)shareDelegate{
    
    return (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
}
-(void)changePositionDownOfBannerView:(NSString *)tag_String
{
    if ([tag_String isEqualToString:BANNER_GOES_DOWN]) {
        [self.bannerView setFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height, self.bannerView.frame.size.width, self.bannerView.frame.size.height)];
    }
    else
    {
        [self.bannerView setFrame:CGRectMake(0, [[UIScreen mainScreen] bounds].size.height-BANNER_HEIGHT, self.bannerView.frame.size.width, self.bannerView.frame.size.height)];
    }

}
- (void)customAlert:(NSString*)title1 withMessage:(NSString*)message
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(title1, title1) message:NSLocalizedString(message, message) delegate:nil
                                          cancelButtonTitle:NSLocalizedString(@"OK", @"OK")
                                          otherButtonTitles:nil];
    [alert show];
    
}
- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
