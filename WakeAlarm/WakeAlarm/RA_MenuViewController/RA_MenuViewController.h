//
//  RA_MenuViewController.h
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/4/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
#import "RA_ProfileViewController.h"
#import "RA_SignUpViewController.h"
#import "RA_LoginViewController.h"
#import "RA_SetAlarmViewController.h"

@interface RA_MenuViewController : UIViewController<OpenProfileViewDelegate>{

    __weak IBOutlet UIView *setAlarmViewContainer;
    __weak IBOutlet UIView *yourProfileViewContainer;
    __weak IBOutlet UIView *scheduleListViewContainer;
    __weak IBOutlet UIView *alarmAFriendViewContainer;
    
 
    __weak IBOutlet UIButton *btnSetAlarm;
    __weak IBOutlet UIButton *btnYourProfile;
    __weak IBOutlet UIButton *btnScheduleList;
    __weak IBOutlet UIButton *btnAlarmAFriend;
    
}
- (IBAction)profileViewAction:(id)sender;
- (IBAction)setAlarmAction:(id)sender;

@end
