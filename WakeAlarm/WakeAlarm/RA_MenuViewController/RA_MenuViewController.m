
//
//  RA_MenuViewController.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/4/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import "RA_MenuViewController.h"

@interface RA_MenuViewController ()

@end

@implementation RA_MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [setAlarmViewContainer setBackgroundColor:RGB(255, 207, 6)];
    [yourProfileViewContainer setBackgroundColor:RGB(255, 143, 8)];
    [scheduleListViewContainer setBackgroundColor:RGB(255, 0, 0)];
    [alarmAFriendViewContainer setBackgroundColor:RGB(168, 32, 32)];
    
    [btnSetAlarm.titleLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:20]];
    [btnYourProfile.titleLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:20]];
    [btnScheduleList.titleLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:20]];
    [btnAlarmAFriend.titleLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:20]];
    
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma -mark button Action
- (IBAction)profileViewAction:(id)sender {
  
   /*
    if (is_Debug) {
        NSDictionary *dataDictionary = [[MyManager sharedManager] retrieveInformationEmailIDWIthPassword:@"onko.cse07@gmail.com" :@"073004"];
        UserInfo *user = [[UserInfo alloc] initWithValueFromDictionary:dataDictionary];
        [[MyManager sharedManager] setActiveUserInformation:user];
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].userID);
    }
   */
    NSLog(@"%@",[[NSUserDefaults standardUserDefaults] valueForKey:@"email"]);
    
    if (([[NSUserDefaults standardUserDefaults] valueForKey:@"email"] != nil)&&([[NSUserDefaults standardUserDefaults] valueForKey:@"password"] != nil)&&([[NSUserDefaults standardUserDefaults] valueForKey:@"username"] != nil)) {
        
        [[MyManager sharedManager] reloadCurrentUserInformationData];
        RA_ProfileViewController *controller = [[RA_ProfileViewController alloc] initWithNibName:@"RA_ProfileViewController" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
    else
    {
        RA_LoginViewController *controller = [[RA_LoginViewController alloc] initWithNibName:@"RA_LoginViewController" bundle:nil];
        controller.delegate = self;
        [self.navigationController pushViewController:controller animated:YES];
    }
}
- (IBAction)setAlarmAction:(id)sender {
    RA_SetAlarmViewController *controller = [[RA_SetAlarmViewController alloc] initWithNibName:@"RA_SetAlarmViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma -mark OpenProfileViewDelegateMethod
-(void)gotoProfileViewFrmLoginScreen:(BOOL)success{
    if (success) {
        RA_ProfileViewController *controller = [[RA_ProfileViewController alloc] initWithNibName:@"RA_ProfileViewController" bundle:nil];
        [self.navigationController pushViewController:controller animated:YES];
    }
}
@end
