//
//  UIBarButtonItem+CustomBackground.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/5/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import "UIBarButtonItem+CustomBackground.h"

@implementation UIBarButtonItem (CustomBackground)

+ (UIBarButtonItem*)barItemWithImage:(UIImage*)image target:(id)target action:(SEL)action
{
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    [button setImage:image forState:UIControlStateNormal];
    [button setFrame:CGRectMake(0.0, 0.0, image.size.width, image.size.height)];
    [button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [[UIBarButtonItem alloc] initWithCustomView:button];
}
@end
