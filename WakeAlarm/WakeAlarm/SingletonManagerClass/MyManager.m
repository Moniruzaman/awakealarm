//
//  MyManager.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 12/27/15.
//  Copyright © 2015 Rasel_cse07. All rights reserved.
//

#import "MyManager.h"

@implementation MyManager
@synthesize activeUserInformation;

#pragma mark Singleton Methods

/*
+(id)sharedManager
{
    static MyManager *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken,^{
        sharedMyManager = [[self alloc] init];
    });

    return sharedMyManager;
}
 */

-(id)init
{
    if (self = [super init]) {
       //initialize default value
    }
    return self;
}

#pragma mark Singleton Methods without GCD dispatch
+(id)sharedManager
{
    static MyManager *sharedMyManager = nil;
    @synchronized(self) {
        if (sharedMyManager == nil) {
            sharedMyManager = [[self alloc] init];
        }
    }
    return sharedMyManager;
}

#pragma -mark singletionMethod
- (NSString *)getDicrectoryPath
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
   return [paths objectAtIndex:0];
    
}
#pragma -mark write Data
-(BOOL)writeInformationEmailIDWithPassword:(NSString *)emailID :(NSString *)password :(NSDictionary *)dataDic{

    NSString *informationPath = [[self getDicrectoryPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@.plist",emailID,password]];
    NSError *error = nil;
    if (informationPath) {
        [[NSFileManager defaultManager] removeItemAtPath:informationPath error:&error];
    }
    return [dataDic writeToFile:informationPath atomically:YES];
}
#pragma -mark readData
-(NSDictionary *)retrieveInformationEmailIDWIthPassword:(NSString *)emailID :(NSString *)password
{
   NSString *informationPath = [[self getDicrectoryPath] stringByAppendingPathComponent:[NSString stringWithFormat:@"%@-%@.plist",emailID,password]];
    if (informationPath) {
        NSDictionary *plistDic = [[NSDictionary alloc] initWithContentsOfFile:informationPath];
        return plistDic;
    }
    else{
        
        [[AppDelegate shareDelegate] customAlert:@"Error!" withMessage:@"No such specific path is found"];
    }
    
    return nil;
}
#pragma -mark Edit Section

-(BOOL)editUserImageEmailIDWIthPassword:(NSString *)emailID :(NSString *)password :(NSData *)imageData
{

    NSMutableDictionary *dataDict = (NSMutableDictionary *)[self retrieveInformationEmailIDWIthPassword:emailID :password];
    [dataDict removeObjectForKey:@"profileIamgeData"];
    [dataDict setObject:imageData forKey:@"profileIamgeData"];
    [dataDict removeObjectForKey:@"imagesetstatus"];
    [dataDict setObject:@"Set" forKey:@"imagesetstatus"];
    return [self writeInformationEmailIDWithPassword:emailID :password :dataDict];
}
-(BOOL)editUserFirstNameLastname:(NSString *)new_fname :(NSString *)new_lname
{
    NSMutableDictionary *dataDict = (NSMutableDictionary *)[self retrieveInformationEmailIDWIthPassword:[[NSUserDefaults standardUserDefaults] valueForKey:@"email"] :[[NSUserDefaults standardUserDefaults] valueForKey:@"password"]];
    [dataDict removeObjectForKey:@"fname"];
    [dataDict setObject:new_fname forKey:@"fname"];
    [dataDict removeObjectForKey:@"lname"];
    [dataDict setObject:new_lname forKey:@"lname"];
    return [self writeInformationEmailIDWithPassword:[[NSUserDefaults standardUserDefaults] valueForKey:@"email"] :[[NSUserDefaults standardUserDefaults] valueForKey:@"password"] :dataDict];
}
-(BOOL)editUserProfileStatus:(NSString *)new_profileStatus
{
    NSMutableDictionary *dataDict = (NSMutableDictionary *)[self retrieveInformationEmailIDWIthPassword:[[NSUserDefaults standardUserDefaults] valueForKey:@"email"] :[[NSUserDefaults standardUserDefaults] valueForKey:@"password"]];
    [dataDict removeObjectForKey:@"profileStatus"];
    [dataDict setObject:new_profileStatus forKey:@"profileStatus"];
    return [self writeInformationEmailIDWithPassword:[[NSUserDefaults standardUserDefaults] valueForKey:@"email"] :[[NSUserDefaults standardUserDefaults] valueForKey:@"password"] :dataDict];
}
#pragma -mark reload data

-(void)reloadCurrentUserInformationData
{
    NSDictionary *dataDict = [self retrieveInformationEmailIDWIthPassword:[[NSUserDefaults standardUserDefaults] valueForKey:@"email"] :[[NSUserDefaults standardUserDefaults] valueForKey:@"password"]];
    UserInfo *user = [[UserInfo alloc] initWithValueFromDictionary:dataDict];
    [[MyManager sharedManager] setActiveUserInformation:user];
}

#pragma -mark ValidateSection
- (BOOL) validateEmail: (NSString *) candidate {
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}
@end
