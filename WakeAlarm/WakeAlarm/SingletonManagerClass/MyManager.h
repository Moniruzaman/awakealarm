//
//  MyManager.h
//  WakeAlarm
//
//  Created by SM Moniruzaman on 12/27/15.
//  Copyright © 2015 Rasel_cse07. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "UserInfo.h"
#import "PrefixHeader.pch"
@interface MyManager : NSObject{

  
}
@property(nonatomic,retain) UserInfo *activeUserInformation;

+(id)sharedManager;
-(NSString *)getDicrectoryPath;
-(BOOL)writeInformationEmailIDWithPassword:(NSString *)emailID :(NSString *)password :(NSDictionary *)dataDic;

-(NSDictionary *)retrieveInformationEmailIDWIthPassword:(NSString *)emailID :(NSString *)password;

-(BOOL)editUserImageEmailIDWIthPassword:(NSString *)emailID :(NSString *)password :(NSData *)imageData;
-(BOOL)editUserFirstNameLastname:(NSString *)new_fname :(NSString *)new_lname;
-(BOOL)editUserProfileStatus:(NSString *)new_profileStatus;

-(void)reloadCurrentUserInformationData;

- (BOOL)validateEmail:(NSString *)candidate;
@end
