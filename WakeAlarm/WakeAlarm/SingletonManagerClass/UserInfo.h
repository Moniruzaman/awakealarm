//
//  UserInfo.h
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/5/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UserInfo : NSObject

@property(nonatomic,retain) NSString *userID;
@property(nonatomic,retain) NSString *firstName;
@property(nonatomic,retain) NSString *lastName;
@property(nonatomic,retain) NSString *gender;
@property(nonatomic,retain) NSString *email;
@property(nonatomic,retain) NSString *password;
@property(nonatomic,retain) NSString *userName;
@property(nonatomic,retain) NSString *userAddress;
@property(nonatomic,retain) NSString *profileStatus;
@property(nonatomic,retain) NSString *totalActiveschedule;
@property(nonatomic,retain) NSString *totalFriend;
@property(nonatomic,retain) NSString *totalAlarmcircleFriend;
//@property(nonatomic,retain) NSString *unlocPattern;
@property(nonatomic,retain) NSData *profileImageData;
@property(nonatomic,retain) NSString *profileImageSetStatus;

-(id)initWithValueFromDictionary :(NSDictionary *)dic;
@end
