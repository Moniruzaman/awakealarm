//
//  UserInfo.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/5/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import "UserInfo.h"

@implementation UserInfo
@synthesize userID,firstName,lastName,gender,email,password,userName,userAddress,profileStatus,totalFriend,totalActiveschedule,totalAlarmcircleFriend,profileImageData,profileImageSetStatus;

-(id)initWithValueFromDictionary :(NSDictionary *)dic
{
    self.userID = [NSString stringWithFormat:@"%@",[dic valueForKey:@"id"]];
    self.firstName = [NSString stringWithFormat:@"%@",[dic valueForKey:@"fname"]];
    self.lastName = [NSString stringWithFormat:@"%@",[dic valueForKey:@"lname"]];
    self.gender = [NSString stringWithFormat:@"%@",[dic valueForKey:@"sex"]];
    self.userName = [NSString stringWithFormat:@"%@",[dic valueForKey:@"username"]];
    self.email = [NSString stringWithFormat:@"%@",[dic valueForKey:@"email"]];
    self.password = [NSString stringWithFormat:@"%@",[dic valueForKey:@"password"]];
    self.userAddress = [NSString stringWithFormat:@"%@",[dic valueForKey:@"address"]];
    self.profileStatus = [NSString stringWithFormat:@"%@",[dic valueForKey:@"profileStatus"]];
    self.totalActiveschedule = [NSString stringWithFormat:@"%@",[dic valueForKey:@"tactiveschedule"]];
    self.totalFriend = [NSString stringWithFormat:@"%@",[dic valueForKey:@"tfriend"]];
    self.totalAlarmcircleFriend = [NSString stringWithFormat:@"%@",[dic valueForKey:@"talarmfriend"]];
    self.profileImageSetStatus = [NSString stringWithFormat:@"%@",[dic valueForKey:@"imagesetstatus"]];
    
    self.profileImageData = [dic valueForKey:@"profileIamgeData"];
    
    return self;
}
@end
