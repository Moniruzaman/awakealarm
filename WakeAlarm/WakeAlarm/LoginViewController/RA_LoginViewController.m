//
//  RA_LoginViewController.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/8/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import "RA_LoginViewController.h"

@interface RA_LoginViewController ()

@end

@implementation RA_LoginViewController
@synthesize delegate;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initialViewSetUp];
}

-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:RGB(255, 0, 0)];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [self setTitle:LOG_IN_VIEW];
    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSFontAttributeName: [UIFont fontWithName:@"Montserrat-Light" size:20.0f],NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    self.navigationItem.leftBarButtonItem  = [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"LBarImage.png"] target:self action:@selector(leftBarButtonItemAction)];
    
}
-(void)initialViewSetUp{
    
    [navBarBottomView setBackgroundColor:RGB(168, 32, 32)];
    [self.view setBackgroundColor:RGB(255, 0, 0)];
    
    [btnLogin.layer setCornerRadius:3.00];
    [btnLogin setClipsToBounds:YES];
    [btnLogin setBackgroundColor:RGB(253, 206, 12)];
    [btnLogin.titleLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:18]];
    [btnLogin setTitleColor:RGB(168, 32, 32) forState:UIControlStateNormal];
    
    [btnSignUp.layer setCornerRadius:3.00];
    [btnSignUp setClipsToBounds:YES];
    [btnSignUp setBackgroundColor:RGB(253, 206, 12)];
    [btnSignUp.titleLabel setFont:[UIFont fontWithName:@"Montserrat-Light" size:18]];
    [btnSignUp setTitleColor:RGB(168, 32, 32) forState:UIControlStateNormal];
    
    for (NSObject *obj in [self.view subviews]) {
        if ([obj isKindOfClass:[UILabel class]]) {
            UILabel *lbl = (UILabel *)obj;
            [lbl setFont:[UIFont fontWithName:@"Montserrat-Light" size:15]];
        }
        else if ([obj isKindOfClass:[UITextField class]])
        {
            UITextField *txt = (UITextField *)obj;
            txt.delegate = self;
            [txt setTextColor:RGB(168, 32, 32)];
        }
    }

}
#pragma -mark UITextFieldDelegateMethod
-(BOOL) textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}
#pragma -mark barButtonActions
-(void)leftBarButtonItemAction
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)logInBtnAction:(id)sender {
    if (!([txtEmailID.text isEqualToString:@""]&&[txtPassword.text isEqualToString:@""])) {
        if ([[MyManager sharedManager] validateEmail:txtEmailID.text]) {
            NSDictionary *userInfoDic = [[MyManager sharedManager] retrieveInformationEmailIDWIthPassword:txtEmailID.text :txtPassword.text];
            if (userInfoDic != nil) {
                UserInfo *user = [[UserInfo alloc] initWithValueFromDictionary:userInfoDic];
                [[MyManager sharedManager] setActiveUserInformation:user];
                
                if (is_Debug) {
                    NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].userName);
                }
                //Store EmailID and Password to Cash NSuserDefault
                [[NSUserDefaults standardUserDefaults] setObject:[[MyManager sharedManager] activeUserInformation].email forKey:@"email"];
                [[NSUserDefaults standardUserDefaults] setObject:[[MyManager sharedManager] activeUserInformation].password forKey:@"password"];
                [[NSUserDefaults standardUserDefaults] setObject:[[MyManager sharedManager] activeUserInformation].userName forKey:@"username"];
                
                [self.navigationController popViewControllerAnimated:YES];
                [self.delegate gotoProfileViewFrmLoginScreen:YES];
            }
            else
            {
                [[AppDelegate shareDelegate] customAlert:@"Error!" withMessage:@"No specific User found!!!!!"];
            }
            
        }
        else
        {
           [[AppDelegate shareDelegate] customAlert:@"Warning!" withMessage:@"Please provide your email in a correct format"];
        }
    }
    else
    {
        [[AppDelegate shareDelegate] customAlert:@"Warning!" withMessage:@"Please type your emailId and password"];
    }
}

- (IBAction)signUpBtnAction:(id)sender {
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(goBackToMenuView) name:GOTO_MENU_VIEW_AFTER_SIGNUP_DONE object:nil];
    RA_SignUpViewController *controller = [[RA_SignUpViewController alloc] initWithNibName:@"RA_SignUpViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}
#pragma -mark postNotificationMethod
-(void)goBackToMenuView
{
    RA_MenuViewController *controller = [[RA_MenuViewController alloc] initWithNibName:@"RA_MenuViewController" bundle:nil];
    [self.navigationController pushViewController:controller animated:YES];
}
@end
