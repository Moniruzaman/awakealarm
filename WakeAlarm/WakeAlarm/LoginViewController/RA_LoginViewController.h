//
//  RA_LoginViewController.h
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/8/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

@protocol OpenProfileViewDelegate;
#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
#import "RA_LoginViewController.h"
#import "RA_SignUpViewController.h"

@class RA_MenuViewController;

@protocol OpenProfileViewDelegate <NSObject>

-(void)gotoProfileViewFrmLoginScreen:(BOOL)success;

@end


@interface RA_LoginViewController : UIViewController<UITextFieldDelegate>{
  __weak IBOutlet UIView *navBarBottomView;
    __weak IBOutlet UITextField *txtEmailID;
    __weak IBOutlet UITextField *txtPassword;
    __weak IBOutlet UIButton *btnLogin;
    __weak IBOutlet UIButton *btnSignUp;
    
}

@property(nonatomic,strong) id<OpenProfileViewDelegate> delegate;

- (IBAction)logInBtnAction:(id)sender;
- (IBAction)signUpBtnAction:(id)sender;

@end
