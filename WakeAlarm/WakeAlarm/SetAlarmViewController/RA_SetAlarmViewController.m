//
//  RA_SetAlarmViewController.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/9/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import "RA_SetAlarmViewController.h"

@interface RA_SetAlarmViewController ()

@end

@implementation RA_SetAlarmViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initialViewSetUp];
    dateLabel.text = [NSDate getTodayDateWithStringFomate];
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:RGB(255, 0, 0)];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [self setTitle:SET_ALARM_TAB];
    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSFontAttributeName: [UIFont fontWithName:@"Montserrat-Light" size:20.0f],NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTranslucent:YES];
    
    self.navigationItem.leftBarButtonItem  = [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"LBarImage.png"] target:self action:@selector(leftBarButtonItemAction)];
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"rBarImage.png"] target:self action:@selector(rightBarButtonItemAction)];
    
}
-(void)initialViewSetUp{
    
    [navBarBottomView setBackgroundColor:RGB(168, 32, 32)];
    [topViewContainer setBackgroundColor:RGB(255, 0, 0)];
    [setContentView setBackgroundColor:RGB(249, 246, 237)];
    [setUnlockerView setBackgroundColor:RGB(249, 246, 237)];
    [setRepetitionView setBackgroundColor:RGB(249, 246, 237)];
    
    [dateLabel setFont:[UIFont fontWithName:@"Montserrat-SemiBold" size:13]];
    [dateLabel setTextColor:RGB(168, 32, 32)];
    [alarmContent setFont:[UIFont fontWithName:@"Montserrat-Light" size:15]];
    
}
#pragma -mark barButtonActions
-(void)leftBarButtonItemAction
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)rightBarButtonItemAction
{
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
- (IBAction)btnAlarmContentAction:(id)sender {
    UIAlertView *alart = [[UIAlertView alloc] initWithTitle:@"Set Alarm Content" message:@"Please Provide your new alarm content message." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alart setAlertViewStyle:UIAlertViewStylePlainTextInput];
    alart.tag = 1;
    
    // Alert style customization
    [[alart textFieldAtIndex:0] setPlaceholder:@"Status message"];
    [alart show];
    
    
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Title" message:@"Message" preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    [alertController addAction:ok];
    
    //[self presentViewController:alertController animated:YES completion:nil];
    
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1) {//For change UserName
        
        if (is_Debug) {
            NSLog(@"1 %@", [alertView textFieldAtIndex:0].text);
        }
        if (buttonIndex==1) {
            if (![[alertView textFieldAtIndex:0].text isEqualToString:@""]) {
                alarmContent.text = [alertView textFieldAtIndex:0].text;
            }
            else
            {
                [[AppDelegate shareDelegate] customAlert:@"Warning!" withMessage:@"Please provide required information."];
            }
        }
    }
}
@end
