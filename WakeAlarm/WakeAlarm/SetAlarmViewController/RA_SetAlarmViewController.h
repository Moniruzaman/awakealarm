//
//  RA_SetAlarmViewController.h
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/9/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
#import "NSDate+CustomDateFormate.h"

@interface RA_SetAlarmViewController : UIViewController{

    __weak IBOutlet UIView *navBarBottomView;
    __weak IBOutlet UIView *topViewContainer;
    __weak IBOutlet UIView *setContentView;
    __weak IBOutlet UIView *setUnlockerView;
    __weak IBOutlet UIView *setRepetitionView;

    __weak IBOutlet UILabel *dateLabel;
    __weak IBOutlet UILabel *alarmContent;

}
- (IBAction)btnAlarmContentAction:(id)sender;

@end
