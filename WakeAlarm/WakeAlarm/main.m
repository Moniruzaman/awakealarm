//
//  main.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/4/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
