//
//  AppDelegate.h
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/4/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import "UserInfo.h"
#import "MyManager.h"

@import GoogleMobileAds;
@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
}
@property(nonatomic,strong) GADBannerView  *bannerView;
@property (strong, nonatomic) UIWindow *window;
+ (AppDelegate*)shareDelegate;
- (void)customAlert:(NSString*)title1 withMessage:(NSString*)message;
-(void)changePositionDownOfBannerView:(NSString *)tag_String;


@end

