//
//  RA_ProfileViewController.m
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/5/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import "RA_ProfileViewController.h"

@interface RA_ProfileViewController ()

@end

@implementation RA_ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self initialViewSetUp];
    [self LoadData];
}
-(void)LoadData
{
    [lblStatusUsername setText:[NSString stringWithFormat:@"%@ %@",[[MyManager sharedManager] activeUserInformation].firstName,[[MyManager sharedManager] activeUserInformation].lastName]];
    [lblStatusAlarmCircle setText:[NSString stringWithFormat:@"%@ Friends",[[MyManager sharedManager] activeUserInformation].totalAlarmcircleFriend]];
    [lblProfileStatus setText:[[MyManager sharedManager] activeUserInformation].profileStatus];
    [lblStatusSchedule setText:[NSString stringWithFormat:@"%@ Schedule",[[MyManager sharedManager] activeUserInformation].totalActiveschedule]];
    [lblStatusTotalFriend setText:[NSString stringWithFormat:@"%@ Friends",[[MyManager sharedManager] activeUserInformation].totalFriend]];
    [lblStatusChangeAvatar setText:[[MyManager sharedManager] activeUserInformation].profileImageSetStatus];
    
    UIImage *img = [UIImage imageWithData:[[MyManager sharedManager] activeUserInformation].profileImageData];
    [profilePictureImageView setImage:img];

}
- (void) touchesBegan: (NSSet *) touches withEvent: (UIEvent *) event {
    UITouch *touch = [touches anyObject];
    if([touch view] == lblProfileStatus) {
        if (is_Debug) {
            NSLog(@"I've been touched");
        }
        UIAlertView *alart = [[UIAlertView alloc] initWithTitle:@"Change Profile Status" message:@"Please Provide your new profile status message." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        [alart setAlertViewStyle:UIAlertViewStylePlainTextInput];
        alart.tag = 2;
        
        // Alert style customization
        [[alart textFieldAtIndex:1] setSecureTextEntry:NO];
        [[alart textFieldAtIndex:0] setPlaceholder:@"Status message"];
        [alart show];
    }
}
-(void)viewWillAppear:(BOOL)animated{
    self.navigationController.navigationBarHidden = NO;
    [self.navigationController.navigationBar setTranslucent:NO];
    [self.navigationController.navigationBar setBarTintColor:RGB(255, 0, 0)];
    [self.navigationItem setHidesBackButton:YES animated:NO];
    [self setTitle:YOUR_PROFILE_TAB];
    [self.navigationController.navigationBar setTitleTextAttributes:@{ NSFontAttributeName: [UIFont fontWithName:@"Montserrat-Light" size:20.0f],NSForegroundColorAttributeName: [UIColor whiteColor]}];
    [self.navigationController.navigationBar setTranslucent:YES];
    self.navigationItem.leftBarButtonItem = [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"LBarImage.png"] target:self action:@selector(leftBarButtonItemAction)];
    
    self.navigationItem.rightBarButtonItem = [UIBarButtonItem barItemWithImage:[UIImage imageNamed:@"logout.png"] target:self action:@selector(rightBarButtonItemAction)];
    //[self.navigationController.navigationBar setTintColor:RGB(255, 0, 0)]; //use to change color of tint back option
    
    if (is_Debug) {
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].userID);
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].firstName);
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].lastName);
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].gender);
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].userName);
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].userAddress);
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].profileStatus);
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].totalActiveschedule);
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].totalFriend);
        NSLog(@"%@",[[MyManager sharedManager] activeUserInformation].totalAlarmcircleFriend);
    }
    
}
#pragma -mark barButtonActions
-(void)leftBarButtonItemAction
{
    [self.navigationController popViewControllerAnimated:YES];
    
}
-(void)rightBarButtonItemAction
{
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"email"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"password"];
    [[NSUserDefaults standardUserDefaults] setObject:nil forKey:@"username"];
    [[MyManager sharedManager] setActiveUserInformation:nil];
    [[AppDelegate shareDelegate] customAlert:@"Success!" withMessage:@"You have been successfully logged out."];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)initialViewSetUp{

    [navBarBottomView setBackgroundColor:RGB(168, 32, 32)];
    [profilePicContainerView setBackgroundColor:RGB(255, 0, 0)];
    [changeUsernameContainer setBackgroundColor:RGB(249, 246, 237)];
    [setPatternContainer setBackgroundColor:RGB(249, 246, 237)];
    
    [lblChangeAvatar setFont:[UIFont fontWithName:@"Montserrat-Light" size:18]];
    [lblChangeAvatar setTextColor:RGB(255, 0, 0)];
    [lblChangeUserName setFont:[UIFont fontWithName:@"Montserrat-Light" size:18]];
    [lblChangeUserName setTextColor:RGB(255, 0, 0)];
    [lblAlarmCircle setFont:[UIFont fontWithName:@"Montserrat-Light" size:18]];
    [lblAlarmCircle setTextColor:RGB(255, 0, 0)];
    [lblUnlockPattern setFont:[UIFont fontWithName:@"Montserrat-Light" size:18]];
    [lblUnlockPattern setTextColor:RGB(255, 0, 0)];
    
    [lblStatusChangeAvatar setFont:[UIFont fontWithName:@"Montserrat-Light" size:13]];
    [lblStatusChangeAvatar setTextColor:RGB(56, 57, 58)];
    [lblStatusUsername setFont:[UIFont fontWithName:@"Montserrat-Light" size:13]];
    [lblStatusUsername setTextColor:RGB(56, 57, 58)];
    [lblStatusAlarmCircle setFont:[UIFont fontWithName:@"Montserrat-Light" size:13]];
    [lblStatusAlarmCircle setTextColor:RGB(56, 57, 58)];
    [lblStatusPattern setFont:[UIFont fontWithName:@"Montserrat-Light" size:13]];
    [lblStatusPattern setTextColor:RGB(56, 57, 58)];
    
    [lblProfileStatus setFont:[UIFont fontWithName:@"Montserrat-Light" size:16]];
    [lblProfileStatus setTextColor:RGB(255, 207, 6)];
    [lblInformationStatus setFont:[UIFont fontWithName:@"Montserrat-Light" size:12]];
    [lblStatusSchedule setFont:[UIFont fontWithName:@"Montserrat-Light" size:16]];
    [lblStatusTotalFriend setFont:[UIFont fontWithName:@"Montserrat-Light" size:16]];
    
    //[profilePictureImageView setBackgroundColor:[UIColor greenColor]];
    [profilePictureImageView.layer setCornerRadius:65];
    [profilePictureImageView setClipsToBounds:YES];
    [profilePictureImageView.layer setBorderWidth:10];
    [profilePictureImageView.layer setBorderColor:RGB(255, 207, 6).CGColor];
    

}
#pragma -mark imageCaptureAction
- (IBAction)btnImagePickAction:(id)sender {
    
    UIAlertView *alart = [[UIAlertView alloc] initWithTitle:@"Information!"
                                                        message:@"How would you like to pic your picture?"
                                                       delegate:self
                                              cancelButtonTitle:@"Camera"
                                              otherButtonTitles:@"Library", nil];
    
    [alart show];
    
}

- (IBAction)btnChangeUserName:(id)sender {
    
    UIAlertView *alart = [[UIAlertView alloc] initWithTitle:@"Change Username" message:@"Please Provide your new Firstname and Lastname" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
    [alart setAlertViewStyle:UIAlertViewStyleLoginAndPasswordInput];
    alart.tag = 1;
    
    // Alert style customization
    [[alart textFieldAtIndex:1] setSecureTextEntry:NO];
    [[alart textFieldAtIndex:0] setPlaceholder:@"Firstname"];
    [[alart textFieldAtIndex:1] setPlaceholder:@"Lastname"];
    [alart show];
    
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    
    if (alertView.tag == 1) {//For change UserName
        
        if (is_Debug) {
            NSLog(@"1 %@", [alertView textFieldAtIndex:0].text);
            NSLog(@"2 %@", [alertView textFieldAtIndex:1].text);
        }
        NSString *fname    = [alertView textFieldAtIndex:0].text;
        NSString *lastname = [alertView textFieldAtIndex:1].text;
        
        if (buttonIndex==1) {
            if (!([fname isEqualToString:@""]&&[lastname isEqualToString:@""])) {
                [[MyManager sharedManager] editUserFirstNameLastname:fname :lastname];
                [[MyManager sharedManager] reloadCurrentUserInformationData];
                [self LoadData];
            }
            else
            {
                [[AppDelegate shareDelegate] customAlert:@"Warning!" withMessage:@"Please provide required information."];
            }
        }
    }
    else if (alertView.tag == 2){//for profile status change
        if (is_Debug) {
            NSLog(@"1 %@", [alertView textFieldAtIndex:0].text);
        }
        NSString *profileStatus    = [alertView textFieldAtIndex:0].text;
        if (buttonIndex==1) {
            if (!([profileStatus isEqualToString:@""])) {
                [[MyManager sharedManager] editUserProfileStatus:profileStatus];
                [[MyManager sharedManager] reloadCurrentUserInformationData];
                [self LoadData];
            }
            else
            {
                [[AppDelegate shareDelegate] customAlert:@"Warning!" withMessage:@"Please provide required information."];
            }
        }
    }
    else
    {
        
        [[AppDelegate shareDelegate] changePositionDownOfBannerView:BANNER_GOES_DOWN];
        if(buttonIndex == 1){ //Library pressed
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            
            [self presentViewController:picker animated:YES completion:NULL];
        }
        else if(buttonIndex == 0){//camera pressed
            
            BOOL cameraAvailable =[UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera];
            if(!cameraAvailable) {
                // running on simulator or something else without camera
                NSLog(@"camera is not found on this device");
            }
            else {
                // camera exists, you can alert
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                [self presentViewController:picker animated:YES completion:NULL];
                
            }
        }
    }
}
#pragma -mark UiImagepickerDelegate
- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    profilePictureImageView.image = chosenImage;
    NSData *webData = UIImagePNGRepresentation(chosenImage);
    
    [[MyManager sharedManager] editUserImageEmailIDWIthPassword:[[MyManager sharedManager] activeUserInformation].email :[[MyManager sharedManager] activeUserInformation].password :webData] ? [[AppDelegate shareDelegate] customAlert:@"Success!" withMessage:@"Image Has been Write successfully"] : [[AppDelegate shareDelegate] customAlert:@"Fail!" withMessage:@"Error Occoured"];
    [lblStatusChangeAvatar setText:@"Set"];
    [picker dismissViewControllerAnimated:YES completion:NULL];
    [[AppDelegate shareDelegate] changePositionDownOfBannerView:@""];
    
}
- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
