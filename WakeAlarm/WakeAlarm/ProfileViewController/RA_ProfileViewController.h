//
//  RA_ProfileViewController.h
//  WakeAlarm
//
//  Created by SM Moniruzaman on 1/5/16.
//  Copyright © 2016 SM Moniruzaman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PrefixHeader.pch"
#import "UIBarButtonItem+CustomBackground.h"


@interface RA_ProfileViewController : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>
{
    __weak IBOutlet UIView *navBarBottomView;
    __weak IBOutlet UIView *profilePicContainerView;
    __weak IBOutlet UIView *changeUsernameContainer;
    __weak IBOutlet UIView *setPatternContainer;
    
    __weak IBOutlet UILabel *lblChangeAvatar;
    __weak IBOutlet UILabel *lblStatusChangeAvatar;
    __weak IBOutlet UILabel *lblChangeUserName;
    __weak IBOutlet UILabel *lblStatusUsername;
    __weak IBOutlet UILabel *lblAlarmCircle;
    __weak IBOutlet UILabel *lblStatusAlarmCircle;
    __weak IBOutlet UILabel *lblUnlockPattern;
    __weak IBOutlet UILabel *lblStatusPattern;
    
    __weak IBOutlet UILabel *lblProfileStatus;
    __weak IBOutlet UILabel *lblInformationStatus;
    __weak IBOutlet UILabel *lblStatusSchedule;
    __weak IBOutlet UILabel *lblStatusTotalFriend;
    __weak IBOutlet UIImageView *profilePictureImageView;

}
- (IBAction)btnImagePickAction:(id)sender;
- (IBAction)btnChangeUserName:(id)sender;

@end
